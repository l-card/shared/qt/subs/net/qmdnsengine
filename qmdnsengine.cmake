set(QMDNSENGINE_DIR ${CMAKE_CURRENT_LIST_DIR}/src)


configure_file(${QMDNSENGINE_DIR}/qmdnsengine_export.h.in "${CMAKE_CURRENT_BINARY_DIR}/qmdnsengine_export.h")

set(QMDNSENGINE_GENFILES "${CMAKE_CURRENT_BINARY_DIR}/qmdnsengine_export.h")

set(QMDNSENGINE_SOURCES
    ${QMDNSENGINE_DIR}/include/qmdnsengine/abstractserver.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/bitmap.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/browser.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/cache.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/dns.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/hostname.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/mdns.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/message.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/prober.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/provider.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/query.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/record.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/resolver.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/server.h
    ${QMDNSENGINE_DIR}/include/qmdnsengine/service.h
    ${QMDNSENGINE_DIR}/src/abstractserver.cpp
    ${QMDNSENGINE_DIR}/src/bitmap.cpp
    ${QMDNSENGINE_DIR}/src/bitmap_p.h
    ${QMDNSENGINE_DIR}/src/browser.cpp
    ${QMDNSENGINE_DIR}/src/browser_p.h
    ${QMDNSENGINE_DIR}/src/cache.cpp
    ${QMDNSENGINE_DIR}/src/cache_p.h
    ${QMDNSENGINE_DIR}/src/dns.cpp
    ${QMDNSENGINE_DIR}/src/hostname.cpp
    ${QMDNSENGINE_DIR}/src/hostname_p.h
    ${QMDNSENGINE_DIR}/src/mdns.cpp
    ${QMDNSENGINE_DIR}/src/message.cpp
    ${QMDNSENGINE_DIR}/src/message_p.h
    ${QMDNSENGINE_DIR}/src/prober.cpp
    ${QMDNSENGINE_DIR}/src/prober_p.h
    ${QMDNSENGINE_DIR}/src/provider.cpp
    ${QMDNSENGINE_DIR}/src/provider_p.h
    ${QMDNSENGINE_DIR}/src/query.cpp
    ${QMDNSENGINE_DIR}/src/query_p.h
    ${QMDNSENGINE_DIR}/src/record.cpp
    ${QMDNSENGINE_DIR}/src/record_p.h
    ${QMDNSENGINE_DIR}/src/resolver.cpp
    ${QMDNSENGINE_DIR}/src/resolver_p.h
    ${QMDNSENGINE_DIR}/src/server.cpp
    ${QMDNSENGINE_DIR}/src/server_p.h
    ${QMDNSENGINE_DIR}/src/service.cpp
    ${QMDNSENGINE_DIR}/src/service_p.h
    )

set(QMDNSENGINE_INCLUDE_DIRS     ${QMDNSENGINE_DIR}/include)
    
set(QMDNSENGINE_LIBRARIES   Qt5::Network)

if(WIN32)
    if (Qt5Network_VERSION VERSION_LESS 5.9.0)
        set(QMDNSENGINE_LIBRARIES ${QMDNSENGINE_LIBRARIES} Ws2_32)
        set(QMDNSENGINE_COMPILE_DEFINITIONS ${QMDNSENGINE_COMPILE_DEFINITIONS} QMDNSENGINE_USE_MANUAL_GROUP_JOIN)
    endif()

endif(WIN32)


set(QMDNSENGINE_FILES ${QMDNSENGINE_SOURCES} ${QMDNSENGINE_GENFILES})
