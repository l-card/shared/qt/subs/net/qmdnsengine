/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Nathan Osman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <QtGlobal>

#ifdef Q_OS_UNIX
#  include <cerrno>
#  include <cstring>
#  include <sys/socket.h>
#endif

#include <QHostAddress>
#include <QNetworkInterface>
#include <qmdnsengine/dns.h>
#include <qmdnsengine/mdns.h>
#include <qmdnsengine/message.h>
#include <qmdnsengine/server.h>

#ifdef QMDNSENGINE_USE_MANUAL_GROUP_JOIN
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include "server_p.h"

using namespace QMdnsEngine;

ServerPrivate::ServerPrivate(Server *server, unsigned iface_update_interval) :
    QObject{server}, q{server}, m_timer{server} {
    connect(&m_timer, &QTimer::timeout, this, &ServerPrivate::checkIfaces);

    m_timer.setInterval(iface_update_interval);
    m_timer.setSingleShot(true);
    checkIfaces();
}

ServerPrivate::~ServerPrivate() {
    const QHash<QString, IfaceSockets > &sockets {m_sockets};
    for (const IfaceSockets &sockets : sockets) {
        removeSocket(sockets.ipv4Socket, MdnsIpv4Address);
        removeSocket(sockets.ipv6Socket, MdnsIpv6Address);
    }
}

bool ServerPrivate::bindSocket(QUdpSocket &socket, const QHostAddress &address) {
    // Exit early if the socket is already bound
    if (socket.state() == QAbstractSocket::BoundState) {
        return true;
    }

    // I cannot find the correct combination of flags that allows the socket
    // to bind properly on Linux, so on that platform, we must manually create
    // the socket and initialize the QUdpSocket with it

#ifdef Q_OS_UNIX
    if (!socket.bind(address, MdnsPort, QAbstractSocket::ShareAddress)) {
        int arg = 1;
        if (setsockopt(socket.socketDescriptor(), SOL_SOCKET, SO_REUSEADDR,
                reinterpret_cast<char*>(&arg), sizeof(int))) {
            Q_EMIT q->error(strerror(errno));
            return false;
        }
#endif
        if (!socket.bind(address, MdnsPort, QAbstractSocket::ReuseAddressHint)) {
            Q_EMIT q->error(socket.errorString());
            return false;
        }
#ifdef Q_OS_UNIX
    }
#endif

    return true;
}

void ServerPrivate::cleanup() {
    m_timer.stop();
    for (IfaceSockets &sockets : m_sockets) {
        delete sockets.ipv4Socket;
        delete sockets.ipv6Socket;
    }
    m_sockets.clear();
}

void ServerPrivate::checkIfaces() {
    QStringList remIfaces {m_sockets.keys()};
    const auto interfaces = QNetworkInterface::allInterfaces();
    for (const QNetworkInterface &iface : interfaces) {
        if ((iface.flags() & QNetworkInterface::CanMulticast)
              &&  (iface.flags() & QNetworkInterface::IsUp)
              &&  !(iface.flags() & QNetworkInterface::IsLoopBack))  {
            QString name {iface.name()};
            if (!remIfaces.contains(name)) {
                IfaceSockets sockets;
                sockets.iface = iface;
                sockets.ipv4Socket = createSocket(MdnsIpv4Address, iface);
                sockets.ipv6Socket = createSocket(MdnsIpv6Address, iface);

                m_sockets[name] = sockets;
            }
            remIfaces.removeAll(name);
        }
    }

    if (!remIfaces.isEmpty()) {
        for (const QString &name : remIfaces) {
            IfaceSockets sockets {m_sockets.take(name)};
            removeSocket(sockets.ipv4Socket, MdnsIpv4Address);
            removeSocket(sockets.ipv6Socket, MdnsIpv6Address);
        }
    }
    m_timer.start();
}

void ServerPrivate::onReadyRead() {
    QUdpSocket *socket = qobject_cast<QUdpSocket*>(sender());
    while (socket->hasPendingDatagrams()) {
        QByteArray packet;
        packet.resize(socket->pendingDatagramSize());
        QHostAddress address;
        quint16 port;
        socket->readDatagram(packet.data(), packet.size(), &address, &port);
        // Attempt to decode the packet
        Message message;
        if (fromPacket(packet, message)) {
            message.setAddress(address);
            message.setPort(port);

            /* Для Qt5.6 windows QUdpSocket::multicastInterface() потребляет
             * значительное число ресурсов. Чтобы не вызывать функцию на каждый
             * пакет, сохраняем объект интерфейса при создании сокета в IfaceSockets
             * и тут находим по сокету его сохраненный интерфейс вручную */
            const IfaceSockets *socketsIface {getSocketIface(socket)};
            if (socketsIface) {
                Q_EMIT q->messageReceived(message, socketsIface->iface);
            }
        }
    }
}

const ServerPrivate::IfaceSockets *ServerPrivate::getSocketIface(QUdpSocket *socket) const {
    const auto it {std::find_if(m_sockets.cbegin(), m_sockets.cend(),
                                [socket](const IfaceSockets &sockets){
            return (sockets.ipv4Socket == socket) || (sockets.ipv6Socket == socket);
        })};
    return (it!= m_sockets.cend()) ? &it.value() : nullptr;
}

bool ServerPrivate::socketJoinGroup(QUdpSocket *udpSocket, const QHostAddress &groupAddress, const QNetworkInterface &iface, bool join) {
#ifdef QMDNSENGINE_USE_MANUAL_GROUP_JOIN
    /* для до 5.9 некорректно был реализован joinMulticastGroup, поэтому реализуем
     * для этого случая вручную кодом из более старших версий */
    bool ok = true;
    int level = 0;
    int sockOpt = 0;
    char *sockArg = nullptr;
    int sockArgSize = 0;

    struct ip_mreq mreq4;
    struct ipv6_mreq mreq6;

    if (groupAddress.protocol() == QAbstractSocket::IPv6Protocol) {
        level = IPPROTO_IPV6;
        sockOpt = join ? IPV6_JOIN_GROUP : IPV6_LEAVE_GROUP;
        sockArg = reinterpret_cast<char *>(&mreq6);
        sockArgSize = sizeof(mreq6);
        memset(&mreq6, 0, sizeof(mreq6));
        Q_IPV6ADDR ip6 = groupAddress.toIPv6Address();
        memcpy(&mreq6.ipv6mr_multiaddr, &ip6, sizeof(ip6));
        mreq6.ipv6mr_interface = iface.index();
    } else if (groupAddress.protocol() == QAbstractSocket::IPv4Protocol) {
        level = IPPROTO_IP;
        sockOpt = join ? IP_ADD_MEMBERSHIP : IP_DROP_MEMBERSHIP;
        sockArg = reinterpret_cast<char *>(&mreq4);
        sockArgSize = sizeof(mreq4);
        memset(&mreq4, 0, sizeof(mreq4));
        mreq4.imr_multiaddr.s_addr = htonl(MdnsIpv4Address.toIPv4Address());

        if (iface.isValid()) {
            const QList<QNetworkAddressEntry> addressEntries = iface.addressEntries();
            bool found = false;
            for (const QNetworkAddressEntry &entry : addressEntries) {
                const QHostAddress ip = entry.ip();
                if (ip.protocol() == QAbstractSocket::IPv4Protocol) {
                    mreq4.imr_interface.s_addr = htonl(ip.toIPv4Address());
                    found = true;
                    break;
                }
            }

            if (!found)
                ok = false;

        } else {
            mreq4.imr_interface.s_addr = INADDR_ANY;
        }
    }


    if (ok) {
        ok =  setsockopt(udpSocket->socketDescriptor(), level, sockOpt, sockArg, sockArgSize) != -1;
    }
    return ok;
#else
    return join ? udpSocket->joinMulticastGroup(groupAddress, iface) : udpSocket->leaveMulticastGroup(groupAddress, iface);
#endif
}

QUdpSocket *ServerPrivate::createSocket(const QHostAddress &groupAddress, const QNetworkInterface &iface) {
    QUdpSocket *udpSocket {new QUdpSocket{this}};

    /** @note bind к явному адресу интерфейса не работает на linux (не принимает mcast) */
    const QHostAddress addr {groupAddress.protocol() == QAbstractSocket::IPv6Protocol ? QHostAddress::AnyIPv6 : QHostAddress::AnyIPv4};
    connect(udpSocket, &QUdpSocket::readyRead, this, &ServerPrivate::onReadyRead);
    bool join_ok = bindSocket(*udpSocket, addr);

    if (join_ok) {
        udpSocket->setMulticastInterface(iface);
        join_ok = socketJoinGroup(udpSocket, groupAddress, iface, true);
    }

    if (!join_ok) {
        delete udpSocket;
        udpSocket = nullptr;
    }

    return udpSocket;
}

void ServerPrivate::removeSocket(QUdpSocket *udpSocket, const QHostAddress &groupAddress) {
    if (udpSocket) {
        socketJoinGroup(udpSocket, groupAddress, udpSocket->multicastInterface(), false);
        udpSocket->close();
        delete udpSocket;
    }
}

Server::Server(QObject *parent, unsigned iface_update_interval)
    : AbstractServer{parent},
      d{new ServerPrivate{this, iface_update_interval}} {
}

void Server::sendMessage(const Message &message) {
    /** @todo теперь тут как-то нужно выбрать сокет из интерфейсов. сейчас unicast в любом случае не используем. */
}

void Server::sendMessageToAll(const Message &message) {
    QByteArray packet;
    toPacket(message, packet);

    for (auto it {d->m_sockets.cbegin()}; it != d->m_sockets.cend(); ++it) {
        if (it.value().ipv4Socket) {
            it.value().ipv4Socket->writeDatagram(packet, MdnsIpv4Address, MdnsPort);
        }
        if (it.value().ipv6Socket) {
            it.value().ipv6Socket->writeDatagram(packet, MdnsIpv6Address, MdnsPort);
        }
    }
}

void Server::stop() {
    d->cleanup();
}
