/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Nathan Osman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef QMDNSENGINE_SERVER_P_H
#define QMDNSENGINE_SERVER_P_H

#include <QObject>
#include <QTimer>
#include <QUdpSocket>
#include <QNetworkInterface>

class QHostAddress;

namespace QMdnsEngine {

class Server;

class ServerPrivate : public QObject {
    Q_OBJECT
public:

    explicit ServerPrivate(Server *server, unsigned iface_update_interval);
    ~ServerPrivate() override;

    bool bindSocket(QUdpSocket &socket, const QHostAddress &address);


    QTimer m_timer;
    struct IfaceSockets {
        QUdpSocket *ipv4Socket {nullptr};
        QUdpSocket *ipv6Socket {nullptr};
        QNetworkInterface iface;
    };
    QHash<QString, IfaceSockets > m_sockets;

    void cleanup();
private Q_SLOTS:

    void checkIfaces();
    void onReadyRead();

private:
    const IfaceSockets *getSocketIface(QUdpSocket *socket) const;

    bool socketJoinGroup(QUdpSocket *udpSocket, const QHostAddress &groupAddress, const QNetworkInterface &iface, bool join);
    QUdpSocket *createSocket(const QHostAddress &mcastAddr, const QNetworkInterface &iface);
    void removeSocket(QUdpSocket *udpSocket, const QHostAddress &mcastAddr);
    Server *const q;
};

}

#endif // QMDNSENGINE_SERVER_P_H
